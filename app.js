let hour = document.getElementById("Hour");
let min = document.getElementById("Min");
let sec = document.getElementById("Sec");
let start = document.getElementById("BtnStart");
let reset = document.getElementById("BtnReset");
let pause = document.getElementById("BtnPause");

let Timehour = 0;
let Timemin = 0;
let Timesec = 0;
let runing = false;
let timer;

start.addEventListener("click", Start);
reset.addEventListener("click", Reset);
pause.addEventListener("click", Pause);

function Start() {
    if (!runing) {
        runing = true;
        timer = setInterval(() => {
            Timesec += 1;
            if (Timesec === 60){
                Timesec = 0;
                Timemin += 1;
            };
            if (Timemin === 60) {
                Timemin = 0;
                Timehour += 1;
            };
            if (Timehour === 24) {
                Timehour = 0;
            };
            sec.innerText = Timesec;
            min.innerText = "0"+Timemin+":";
            hour.innerText = "0"+Timehour+":";
            if (Timesec === 10){
                sec.innerTEXT = Timesec;
            }
        }, 1000);
    };
};

function Reset() {
    runing = false;
    clearInterval(timer);
    Timehour = 0;
    Timemin = 0;
    Timesec = 0;
    hour.innerText = Timehour;
    min.innerText = Timemin;
    sec.innerText = Timesec;
};

function Pause() {
    runing = false;
    clearInterval(timer);
};